resource "aws_ecs_cluster" "main" {
  name = var.ecs_cluster_name
  capacity_providers = ["FARGATE"]
}

data "template_file" "template" {
  template = file("${path.module}/templates/ecs/container_definitions.json.tpl")
  vars = var.container_vars
}

resource "aws_ecs_task_definition" "main" {
  family                   = var.task_definition_family_name
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.cpu
  memory                   = var.memory
  container_definitions    = data.template_file.template.rendered
}

resource "aws_ecs_service" "main" {
  depends_on = [aws_lb_listener.main, aws_iam_role_policy_attachment.ecs_task_execution_role]

  name            = var.ecs_service_name
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.main.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"
  health_check_grace_period_seconds  = var.health_check_grace_period_seconds

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks_sg.id]
    subnets          = var.subnets_map
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.main.id
    container_name   = var.container_name
    container_port   = var.app_port
  }
}