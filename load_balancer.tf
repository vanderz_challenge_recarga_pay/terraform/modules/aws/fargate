resource "aws_lb" "main" {
  name            = var.lb_name
  subnets         = var.subnets_map
  load_balancer_type = var.lb_type

  enable_deletion_protection = var.lb_enable_deletion_protection
  internal = var.lb_internal

  tags = var.lb_tags
}

resource "aws_lb_target_group" "main" {
  name        = var.lb_target_group_name
  port        = var.lb_port
  protocol    = var.lb_protocol
  vpc_id      = var.vpc_map
  target_type = "ip"

  health_check {
    healthy_threshold   = var.healthy_threshold
    interval            = var.health_interval
    protocol            = var.lb_protocol
    unhealthy_threshold = var.unhealthy_threshold
  }
}

# Redirect all traffic from the lb to the target group
resource "aws_lb_listener" "main" {
  load_balancer_arn = aws_lb.main.id

  for_each = var.lb_listeners
  port = each.key
  protocol = each.value.protocol
  ssl_policy = each.value.ssl_policy
  certificate_arn = each.value.certificate_arn

  default_action {
    target_group_arn = aws_lb_target_group.main.id
    type             = "forward"
  }
}