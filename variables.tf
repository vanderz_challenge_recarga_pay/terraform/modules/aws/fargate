# network
variable "vpc_map" {}

variable "subnets_map" {}

variable "az_count" {}

# ECS
variable "ecs_cluster_name" {}

variable "ecs_service_name" {}

variable "container_name" {}

variable "container_vars" {}

variable "app_name" {}

variable "app_version" {}

variable "app_image" {}

variable "app_port" {}

variable "app_count" {}

variable "health_check_path" {}

variable "health_check_grace_period_seconds" {}

variable "healthy_threshold" {}

variable "unhealthy_threshold" {}

variable "health_interval" {}

variable "health_matcher" {}

variable "health_timeout" {}

variable "cpu" {}

variable "memory" {}

variable "task_definition_family_name" {}

variable "ecs_sg_ingress_cidr_blocks" {}

variable "ecs_task_execution_role_name" {}

variable "ecs_task_execution_role_policy_arn" {}

variable "ecs_tasks_security_group_name" {}

variable "ecs_tasks_security_group_description" {}

# Load Balancer
variable "lb_name" {}

variable "lb_type" {}

variable "lb_enable_deletion_protection" {}

variable "lb_internal" {}

variable "lb_target_group_name" {}

variable "lb_port" {}

variable "lb_protocol" {}

variable "lb_listeners" {}

variable "lb_tags" {}

# Logging
variable "log_retention_in_days" {}

variable "log_group_name" {}

variable "log_group_tags" {}

variable "log_stream" {}

# Auto Scaling
variable "appautoscaling_target_min_capacity" {}

variable "appautoscaling_target_max_capacity" {}

variable "up_policy_name" {}

variable "up_policy_cooldown" {}

variable "up_policy_metric_aggregation_type" {}

variable "down_policy_name" {}

variable "down_policy_cooldown" {}

variable "down_policy_metric_aggregation_type" {}

variable "service_cpu_high_name" {}

variable "service_cpu_high_comparison_operator" {}

variable "service_cpu_high_evaluation_periods" {}

variable "service_cpu_high_period" {}

variable "service_cpu_high_threshold" {}

variable "service_cpu_high_statistic" {}

variable "service_cpu_low_name" {}

variable "service_cpu_low_comparison_operator" {}

variable "service_cpu_low_evaluation_periods" {}

variable "service_cpu_low_period" {}

variable "service_cpu_low_threshold" {}

variable "service_cpu_low_statistic" {}
