output "aws_lb_dns_name" {
  value       = aws_lb.main.dns_name
  description = "Endpoint do balanceador"
}

output "aws_lb_id" {
  value       = aws_lb.main.id
}

output "ecs_task_execution_role_name" {
  value = aws_iam_role.ecs_task_execution_role.name
}