resource "aws_appautoscaling_target" "main" {
  service_namespace  = "ecs"
  resource_id        = "service/${aws_ecs_cluster.main.name}/${aws_ecs_service.main.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  min_capacity       = var.appautoscaling_target_min_capacity
  max_capacity       = var.appautoscaling_target_max_capacity
}

# Automatically scale capacity up by one
resource "aws_appautoscaling_policy" "up_policy" {
  name               =  var.up_policy_name
  service_namespace  = "ecs"
  resource_id        = "service/${aws_ecs_cluster.main.name}/${aws_ecs_service.main.name}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = var.up_policy_cooldown
    metric_aggregation_type = var.up_policy_metric_aggregation_type

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}

# Automatically scale capacity down by one
resource "aws_appautoscaling_policy" "down_policy" {
  name               = var.down_policy_name
  service_namespace  = "ecs"
  resource_id        = "service/${aws_ecs_cluster.main.name}/${aws_ecs_service.main.name}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = var.down_policy_cooldown
    metric_aggregation_type = var.down_policy_metric_aggregation_type

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = -1
    }
  }
}

# CloudWatch alarm that triggers the autoscaling up policy
resource "aws_cloudwatch_metric_alarm" "service_cpu_high" {
  alarm_name          = var.service_cpu_high_name
  comparison_operator = var.service_cpu_high_comparison_operator
  evaluation_periods  = var.service_cpu_high_evaluation_periods
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = var.service_cpu_high_period
  statistic           = var.service_cpu_high_statistic
  threshold           = var.service_cpu_high_threshold

  dimensions = {
    ClusterName = aws_ecs_cluster.main.name
    ServiceName = aws_ecs_service.main.name
  }

  alarm_actions = [aws_appautoscaling_policy.up_policy.arn]
}

# CloudWatch alarm that triggers the autoscaling down policy
resource "aws_cloudwatch_metric_alarm" "service_cpu_low" {
  alarm_name          = var.service_cpu_low_name
  comparison_operator = var.service_cpu_low_comparison_operator
  evaluation_periods  = var.service_cpu_low_evaluation_periods
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = var.service_cpu_low_period
  statistic           = var.service_cpu_low_statistic
  threshold           = var.service_cpu_low_threshold

  dimensions = {
    ClusterName = aws_ecs_cluster.main.name
    ServiceName = aws_ecs_service.main.name
  }

  alarm_actions = [aws_appautoscaling_policy.down_policy.arn]
}