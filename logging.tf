resource "aws_cloudwatch_log_group" "main" {
  name              = var.log_group_name
  retention_in_days = var.log_retention_in_days

  tags = var.log_group_tags
}

resource "aws_cloudwatch_log_stream" "main" {
  name           = var.log_stream
  log_group_name = aws_cloudwatch_log_group.main.name
}