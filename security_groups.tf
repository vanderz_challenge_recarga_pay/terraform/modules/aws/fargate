resource "aws_security_group" "ecs_tasks_sg" {
  name        = var.ecs_tasks_security_group_name
  description = var.ecs_tasks_security_group_description
  vpc_id      = var.vpc_map

  ingress {
    protocol        = "tcp"
    from_port       = var.app_port
    to_port         = var.app_port
    cidr_blocks = var.ecs_sg_ingress_cidr_blocks
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}